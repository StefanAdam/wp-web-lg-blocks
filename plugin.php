<?php
/**
 * Plugin Name:     LG Blocks
 * Plugin URI:      https://www.longevitygraphics.com
 * Description:     Block Provider for Longevity Themes.
 * Author:          Stefan Adam
 * Author URI:      https://ww.longevitygraphics.com
 * Text Domain:     lg-blocks
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Lg_Blocks
 */

// Your code starts here.
if (!defined('ABSPATH')) {
    exit();
}

function lg_blocks_categories($categories, $post)
{
    return array_merge($categories, array(
        array(
            'slug' => 'lg-blocks-category',
            'title' => __('LG Blocks', 'lg-blocks'),
            'icon' => 'wordpress'
        )
    ));
}

function lg_blocks_register_block_type($block, $options = array())
{
    register_block_type(
        'lg-blocks/' . $block,
        array_merge(
            array(
                'editor_script' => 'lg-blocks-editor-script',
                'editor_style' => 'lg-blocks-editor-style',
                'script' => 'lg-blocks-script',
                'style' => 'lg-blocks-style'
                //
            ),
            $options
        )
    );
}

function lg_blocks_register()
{
    wp_register_script(
        'lg-blocks-editor-script',
        plugins_url('dist/editor.js', __FILE__),
        array(
            'wp-blocks',
            'wp-i18n',
            'wp-element',
            'wp-editor',
            'wp-components',
            'wp-blob',
            'wp-html-entities',
            'lodash'
        )
    );

    wp_register_style(
        'lg-blocks-editor-style',
        plugins_url('dist/editor.css', __FILE__),
        array('wp-edit-blocks')
    );

    wp_register_script(
        'lg-blocks-script',
        plugins_url('dist/script.js', __FILE__),
        array('jquery')
    );

    wp_register_style(
        'lg-blocks-style',
        plugins_url('dist/style.css', __FILE__)
    );

    lg_blocks_register_block_type('firstblock');
    lg_blocks_register_block_type('secondblock');
    lg_blocks_register_block_type('team-member');
    lg_blocks_register_block_type('team-members');
    lg_blocks_register_block_type('latest-posts', array(
        'render_callback' => 'lg_blocks_render_latest_posts_block',
        'attributes' => array(
            'numberOfPosts' => array(
                'type' => 'number',
                'default' => 5
            ),
            'postCategories' => array(
                'type' => 'string'
            )
        )
    ));
}
add_action('init', 'lg_blocks_register');

function lg_blocks_render_latest_posts_block($attributes)
{
    $args = array(
        'posts_per_page' => $attributes['numberOfPosts']
    );
    $query = new WP_Query($args);
    $posts = '';
    if ($query->have_posts()) {
        $posts .=
            '<div class="wp-blocks-lg-blocks-latest-posts has-' .
            $attributes['numberOfPosts'] .
            '-columns ">';
        while ($query->have_posts()) {
            $query->the_post();
            $posts .=
                '<div class="lg-post card">
                    <div class="lg-post__image card-img-top"> ' .
                get_the_post_thumbnail(null, null) .
                '</div>
                    <div class="lg-post__body card-body">
                        <div class="lg-post__title py-2"> ' .
                get_the_title() .
                '</div>
                        <div class="lg-post__exerpt"> ' .
                get_the_excerpt() .
                '</div>
                        <div class="lg-post__link"> ' .
                '<a href="' .
                esc_url(get_the_permalink()) .
                '"> Learn More </a>' .
                '</div>
                    </div>
                </div>';
        }

        $posts .= '</div>';
        wp_reset_postdata();
        return $posts;
    } else {
        return '<div>' . __("No Posts Found", "lg-blocks") . '</div>';
    }
}

add_filter('block_categories', 'lg_blocks_categories', 10, 2);
