import { registerBlockType, createBlock } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";

import { InnerBlocks, InspectorControls } from "@wordpress/editor";
import { PanelBody, RangeControl } from "@wordpress/components";

const attributes = {
    columns: {
        type: "number",
        default: 2,
    },
};

registerBlockType("lg-blocks/team-members", {
    title: __("Team Members", "lg-blocks"),
    description: __("Block for showing a Team Member", "lg-blocks"),
    icon: "grid-view",
    supports: {
        html: false,
        align: ["wide", "full"],
    },
    category: "lg-blocks-category",
    keywords: [
        __("team", "lg-blocks"),
        __("member", "lg-blocks"),
        __("person", "lg-blocks"),
    ],
    transforms: {
        from: [
            {
                type: "block",
                blocks: ["core/gallery"],
                transform: ({ images, columns }) => {
                    let inner = images.map(({ alt, id, url }) => {
                        return createBlock("lg-blocks/team-member", {
                            alt,
                            id,
                            url,
                        });
                    });
                    return createBlock(
                        "lg-blocks/team-members",
                        {
                            columns: columns,
                        },
                        inner
                    );
                },
            },
            {
                type: "block",
                blocks: ["core/image"],
                isMultiBlock: true,
                transform: (attributes) => {
                    let inner = attributes.map(({ alt, id, url }) => {
                        return createBlock("lg-blocks/team-member", {
                            alt,
                            id,
                            url,
                        });
                    });
                    return createBlock(
                        "lg-blocks/team-members",
                        {
                            columns: attributes.length,
                        },
                        inner
                    );
                },
            },
        ],
    },
    attributes,
    edit({ className, attributes, setAttributes }) {
        const { columns } = attributes;
        return (
            <div className={`${className} has-${columns}-columns`}>
                <InspectorControls>
                    <PanelBody>
                        <RangeControl
                            label={__("Columns", "lg-blocks")}
                            value={columns}
                            onChange={(columns) => setAttributes({ columns })}
                            min={1}
                            max={6}
                        />
                    </PanelBody>
                </InspectorControls>
                <InnerBlocks
                    allowedBlocks={["lg-blocks/team-member"]}
                    template={[
                        ["lg-blocks/team-member"],
                        ["lg-blocks/team-member"],
                    ]}
                />
            </div>
        );
    },

    save({ attributes }) {
        const { columns } = attributes;
        return (
            <div className={`has-${columns}-columns`}>
                <InnerBlocks.Content />
            </div>
        );
    },
});
