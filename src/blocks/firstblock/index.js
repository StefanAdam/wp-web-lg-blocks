var registerBlockType = wp.blocks.registerBlockType;
var __ = wp.i18n.__;
var el = wp.element.createElement;

registerBlockType("lg-blocks/firstblock", {
	title: __("First Block", "lg-blocks"),
	description: __("Our First block", "lg-blocks"),
	category: "layout",
	icon: {
		src: "admin-network"
	},
	keywords: [__("photo", "lg-blocks"), __("image", "lg-blocks")],
	edit: function() {
		return el("p", null, "Editor");
	},
	save: function() {
		return el("p", null, "Saved Contents");
	}
});
