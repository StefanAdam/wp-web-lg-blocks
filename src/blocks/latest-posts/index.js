import "./styles.editor.scss";

import edit from "./edit";
import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";

registerBlockType("lg-blocks/latest-posts", {
    title: __("Latest Posts", "lg=blocks"),
    description: __("Block that shows a list of the latest posts", "lg-blocks"),
    icon: "admin-post",
    category: "lg-blocks-category",
    edit: edit,
    save() {
        return null;
    },
});
