import { Component, RawHTML } from "@wordpress/element";
import { withSelect } from "@wordpress/data";
import { __ } from "@wordpress/i18n";
import { decodeEntities } from "@wordpress/html-entities";
import { RangeControl, PanelBody } from "@wordpress/components";
import { InspectorControls } from "@wordpress/editor";

class LatestPostsEdit extends Component {
    onChangeNumberOfPosts = (numberOfPosts) => {
        this.props.setAttributes({ numberOfPosts });
    };

    render() {
        const { posts, className, attributes } = this.props;
        const { numberOfPosts } = attributes;

        const getFeaturedMedia = (post) => {
            const media = wp.data.select("core").getMedia(post.featured_media);
            if (media) {
                return <img src={media.source_url} alt={media.alt_text} />;
            }
            return <div> Loading... </div>;
        };

        console.log(posts);
        return (
            <>
                <InspectorControls>
                    <PanelBody title={__("Posts Settings", "lg-blocks")}>
                        <RangeControl
                            label={__("Number of Posts", "lg-blocks")}
                            value={numberOfPosts}
                            onChange={this.onChangeNumberOfPosts}
                            min={1}
                            max={10}
                        />
                    </PanelBody>
                </InspectorControls>
                {posts && posts.length > 0 ? (
                    <div
                        className={`${className} has-${numberOfPosts}-columns`}
                    >
                        {posts.map((post) => (
                            <div key={post.id} className="lg-post card">
                                <div className="lg-post__image card-img-top">
                                    {getFeaturedMedia(post)}
                                </div>
                                <div className="lg-post__body card-body">
                                    <div className="lg-post__title py-2">
                                        {decodeEntities(post.title.rendered)}
                                    </div>
                                    <div className="lg-post__exerpt">
                                        <RawHTML>
                                            {post.excerpt.rendered}
                                        </RawHTML>
                                    </div>
                                    <div className="lg-post__link">
                                        <a href={post.link}>Read More</a>
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>
                ) : (
                    <div>
                        {posts
                            ? __("No Posts Found", "lg-blocks")
                            : __("Loading...", "lg-blocks")}
                    </div>
                )}
            </>
        );
    }
}

export default withSelect((select, props) => {
    const { attributes } = props;
    const { numberOfPosts } = attributes;
    let query = { per_page: numberOfPosts };

    return {
        posts: select("core").getEntityRecords("postType", "post", query),
    };
})(LatestPostsEdit);
